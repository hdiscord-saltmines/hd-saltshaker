class HDSarge:HDSkin{
	default{
		hdskin.tauntsound "player/sarge/taunt";
		hdskin.xdeathsound "player/sarge/xdeath";
		hdskin.gruntsound "player/sarge/grunt";
		hdskin.landsound "player/sarge/land";
		hdskin.medsound "player/sarge/meds";
		hdskin.soundclass "sarge";
		deathsound "player/sarge/death";
		painsound "player/sarge/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDBarney:HDSkin{
	default{
		hdskin.tauntsound "player/barney/taunt";
		hdskin.xdeathsound "player/barney/xdeath";
		hdskin.gibbedsound  "player/barney/gibbed";
		hdskin.gruntsound "player/barney/grunt";
		hdskin.landsound "player/barney/land";
		hdskin.medsound "player/barney/meds";
		hdskin.soundclass "barney";
		deathsound "player/barney/death";
		painsound "player/barney/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:BRNY A 0;stop;
	crouch:BRNC A 0;stop;
	fist:BFIS A 0;stop;
	}
}

class HDFGrunt:HDSkin{
	default{
		hdskin.tauntsound "player/fgrunt/taunt";
		hdskin.xdeathsound "player/fgrunt/xdeath";
		hdskin.gibbedsound  "player/fgrunt/gibbed";
		hdskin.gruntsound "player/fgrunt/grunt";
		hdskin.landsound "player/fgrunt/land";
		hdskin.medsound "player/fgrunt/meds";
		hdskin.soundclass "fgrunt";
		deathsound "player/fgrunt/death";
		painsound "player/fgrunt/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:FGRU A 0;stop;
	crouch:FGRU A 0;stop;
	fist:BFIS A 0;stop;
	}
}

class HDOtis:HDSkin{
	default{
		hdskin.tauntsound "player/otis/taunt";
		hdskin.xdeathsound "player/otis/xdeath";
		hdskin.gibbedsound  "player/otis/gibbed";
		hdskin.gruntsound "player/otis/grunt";
		hdskin.landsound "player/otis/land";
		hdskin.medsound "player/otis/meds";
		hdskin.soundclass "otis";
		deathsound "player/otis/death";
		painsound "player/otis/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:BRNY A 0;stop;
	crouch:BRNC A 0;stop;
	fist:BFIS A 0;stop;
	}
}

class HDUSCM:HDSkin{
	default{
		hdskin.tauntsound "player/uscm/taunt";
		hdskin.xdeathsound "player/uscm/xdeath";
		hdskin.gruntsound "player/uscm/grunt";
		hdskin.landsound "player/uscm/land";
		hdskin.medsound "player/uscm/meds";
		hdskin.soundclass "uscm";
		deathsound "player/uscm/death";
		painsound "player/uscm/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDODST:HDSkin{
	default{
		hdskin.tauntsound "player/ODST/taunt";
		hdskin.xdeathsound "player/ODST/xdeath";
		hdskin.gruntsound "player/ODST/grunt";
		hdskin.landsound "player/ODST/land";
		hdskin.medsound "player/ODST/meds";
		hdskin.soundclass "ODST";
		deathsound "player/ODST/death";
		painsound "player/ODST/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDMSlug:HDSkin{
	default{
		hdskin.tauntsound "player/mslug/taunt";
		hdskin.xdeathsound "player/mslug/xdeath";
		hdskin.gruntsound "player/mslug/grunt";
		hdskin.landsound "player/mslug/land";
		hdskin.medsound "player/mslug/meds";
		hdskin.soundclass "mslug";
		deathsound "player/mslug/death";
		painsound "player/mslug/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDMario:HDSkin{
	default{
		hdskin.tauntsound "player/mario/taunt";
		hdskin.xdeathsound "player/mario/xdeath";
		hdskin.gruntsound "player/mario/grunt";
		hdskin.landsound "player/mario/land";
		hdskin.medsound "player/mario/meds";
		hdskin.soundclass "mario";
		deathsound "player/mario/death";
		painsound "player/mario/pain";
		hdskin.mug "MRI";
	}
	states{
	spawn:MARI A 0;stop;
	crouch:MARC A 0;stop;
	fist:MRFI A 0;stop;
	}
}

class HDAlbus:HDSkin{
	default{
		hdskin.tauntsound "player/albus/taunt";
		hdskin.xdeathsound "player/albus/xdeath";
		hdskin.gruntsound "player/albus/grunt";
		hdskin.landsound "player/albus/land";
		hdskin.medsound "player/albus/meds";
		hdskin.soundclass "albus";
		deathsound "player/albus/death";
		painsound "player/albus/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:ALBU A 0;stop;
	crouch:ALBC A 0;stop;
	fist:ALBF A 0;stop;
	}
}

class HDAlucard:HDSkin{
	default{
		hdskin.tauntsound "player/alucard/taunt";
		hdskin.xdeathsound "player/alucard/xdeath";
		hdskin.gruntsound "player/alucard/grunt";
		hdskin.landsound "player/alucard/land";
		hdskin.medsound "player/alucard/meds";
		hdskin.soundclass "alucard";
		deathsound "player/alucard/death";
		painsound "player/alucard/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDShanoa:HDSkin{
	default{
		hdskin.tauntsound "player/shanoa/taunt";
		hdskin.xdeathsound "player/shanoa/xdeath";
		hdskin.gruntsound "player/shanoa/grunt";
		hdskin.landsound "player/shanoa/land";
		hdskin.medsound "player/shanoa/meds";
		hdskin.soundclass "shanoa";
		deathsound "player/shanoa/death";
		painsound "player/shanoa/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDMCVillager:HDSkin{
	default{
		hdskin.tauntsound "player/MCVillager/taunt";
		hdskin.xdeathsound "player/MCVillager/xdeath";
		hdskin.gruntsound "player/MCVillager/grunt";
		hdskin.landsound "player/MCVillager/land";
		hdskin.medsound "player/MCVillager/meds";
		hdskin.soundclass "MCVillager";
		deathsound "player/MCVillager/death";
		painsound "player/MCVillager/pain";
		hdskin.mug "VHT";
	}
	states{
	spawn:VILL A 0;stop;
	crouch:VILL A 0;stop;
	fist:VILF A 0;stop;
	}
}

class HDDeusExBot:HDSkin{
	default{
		hdskin.tauntsound "player/deusexbot/taunt";
		hdskin.xdeathsound "player/deusexbot/xdeath";
		hdskin.gibbedsound "player/deusexbot/gibbed";
		hdskin.gruntsound "player/deusexbot/grunt";
		hdskin.landsound "player/deusexbot/land";
		hdskin.medsound "player/deusexbot/meds";
		hdskin.stepsound "player/deusexbot/step";
		hdskin.stepsoundwet "player/deusexbot/stepwet";
		hdskin.soundclass "deusexbot";
		deathsound "player/deusexbot/death";
		painsound "player/deusexbot/pain";
		hdskin.mug "DBF";
	}
	states{
	spawn:DEXB A 0;stop;
	crouch:DEXC A 0;stop;
	fist:RBPN A 0;stop;
	}
}
class HDThunderBot:HDSkin{
	default{
		hdskin.tauntsound "player/thunderbot/taunt";
		hdskin.xdeathsound "player/thunderbot/zerk";
		hdskin.gibbedsound "player/thunderbot/gibbed";
		hdskin.gruntsound "player/thunderbot/grunt";
		hdskin.landsound "player/thunderbot/land";
		hdskin.medsound "player/thunderbot/meds";
		hdskin.stepsound "player/thunderbot/step";
		hdskin.stepsoundwet "player/thunderbot/stepwet";
		hdskin.soundclass "thunderbot";
		deathsound "player/thunderbot/death";
		painsound "player/thunderbot/pain";
		hdskin.mug "DBF";
		xscale 1.15;
		yscale 1.15;
	}
	states{
	spawn:DEX2 A 0;stop;
	crouch:DEX3 A 0;stop;
	fist:RBPN A 0;stop;
	}
}

class HDThunderBot2:HDSkin{
	default{
		hdskin.tauntsound "player/thunderbot2/taunt";
		hdskin.xdeathsound "player/thunderbot2/zerk";
		hdskin.gibbedsound "player/thunderbot2/gibbed";
		hdskin.gruntsound "player/thunderbot2/grunt";
		hdskin.landsound "player/thunderbot2/land";
		hdskin.medsound "player/thunderbot2/meds";
		hdskin.stepsound "player/thunderbot2/step";
		hdskin.stepsoundwet "player/thunderbot2/stepwet";
		hdskin.soundclass "thunderbot2";
		deathsound "player/thunderbot2/death";
		painsound "player/thunderbot2/pain";
		hdskin.mug "DBF";
		xscale 1.15;
		yscale 1.15;
	}
	states{
	spawn:DEX2 A 0;stop;
	crouch:DEX3 A 0;stop;
	fist:RBPN A 0;stop;
	}
}

class HDHexDrone:HDSkin{
	default{
		hdskin.tauntsound "player/hexdrone/taunt";
		hdskin.xdeathsound "player/hexdrone/zerk";
		hdskin.gibbedsound "player/hexdrone/gibbed";
		hdskin.gruntsound "player/hexdrone/grunt";
		hdskin.landsound "player/hexdrone/land";
		hdskin.medsound "player/hexdrone/meds";
		hdskin.stepsound "player/hexdrone/step";
		hdskin.stepsoundwet "player/hexdrone/stepwet";
		hdskin.soundclass "hexdrone";
		deathsound "player/hexdrone/death";
		painsound "player/hexdrone/pain";
		hdskin.mug "DBF";
		xscale 0.5;
		yscale 0.5;
	}
	states{
	spawn:HEXD A 0;stop;
	crouch:HEXC A 0;stop;
	fist:RBPN A 0;stop;
	}
}

class HDDXSpider:HDSkin{
	default{
		hdskin.tauntsound "player/dxspider/taunt";
		hdskin.xdeathsound "player/dxspider/zerk";
		hdskin.gibbedsound  "player/dxspider/gibbed";
		hdskin.gruntsound "player/dxspider/grunt";
		hdskin.landsound "player/dxspider/land";
		hdskin.medsound "player/dxspider/meds";
		hdskin.stepsound "player/dxspider/step";
		hdskin.stepsoundwet "player/dxspider/stepwet";
		hdskin.soundclass "dxspider";
		deathsound "player/dxspider/death";
		painsound "player/dxspider/pain";
		hdskin.mug "DBF";
//		xscale 0.5;
//		yscale 0.5;
	}
	states{
	spawn:DXSP A 0;stop;
	crouch:DXSC A 0;stop;
	fist:RBPN A 0;stop;
	}
}

class HDPMC:HDSkin{
	default{
		hdskin.tauntsound "player/pmc/taunt";
		hdskin.xdeathsound "player/pmc/xdeath";
		hdskin.gruntsound "player/pmc/grunt";
		hdskin.landsound "player/pmc/land";
		hdskin.medsound "player/pmc/meds";
		hdskin.soundclass "pmc";
		deathsound "player/pmc/death";
		painsound "player/pmc/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

// yes/no responses and occaisonal comments
class HDReplicant:HDSkin{
	default{
		hdskin.tauntsound "player/replicant/response";
		hdskin.xdeathsound "player/replicant/xdeath";
		hdskin.gruntsound "player/replicant/grunt";
		hdskin.landsound "player/replicant/land";
		hdskin.medsound "player/replicant/meds";
		hdskin.soundclass "replicant";
		deathsound "player/replicant/death";
		painsound "player/replicant/pain";
		hdskin.mug "STC";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}
// callouts for after killing an enemy
class HDReplicantKill:HDSkin{
	default{
		hdskin.tauntsound "player/replicant/taunt";
		hdskin.xdeathsound "player/replicant/xdeath";
		hdskin.gruntsound "player/replicant/grunt";
		hdskin.landsound "player/replicant/land";
		hdskin.medsound "player/replicant/meds";
		hdskin.soundclass "replicant";
		deathsound "player/replicant/death";
		painsound "player/replicant/pain";
		hdskin.mug "STC";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}
// warnings when you spot an enemy
class HDReplicantSight:HDSkin{
	default{
		hdskin.tauntsound "player/replicant/sight";
		hdskin.xdeathsound "player/replicant/xdeath";
		hdskin.gruntsound "player/replicant/grunt";
		hdskin.landsound "player/replicant/land";
		hdskin.medsound "player/replicant/meds";
		hdskin.soundclass "replicant";
		deathsound "player/replicant/death";
		painsound "player/replicant/pain";
		hdskin.mug "STC";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}
// shut up coomer
class HDReplicantShutUp:HDSkin{
	default{
		hdskin.tauntsound "player/replicant/shutup";
		hdskin.xdeathsound "player/replicant/xdeath";
		hdskin.gruntsound "player/replicant/grunt";
		hdskin.landsound "player/replicant/land";
		hdskin.medsound "player/replicant/meds";
		hdskin.soundclass "replicant";
		deathsound "player/replicant/death";
		painsound "player/replicant/pain";
		hdskin.mug "STC";
	}
	states{
	spawn:PLCM A 0;stop;
	crouch:PCCM A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDCloaker:HDSkin{
	default{
		hdskin.tauntsound "player/cloaker/taunt";
		hdskin.xdeathsound "player/cloaker/zerk";
		hdskin.gruntsound "player/cloaker/grunt";
		hdskin.landsound "player/cloaker/land";
		hdskin.medsound "player/cloaker/stim";
		hdskin.soundclass "cloaker";
		deathsound "player/cloaker/death";
		painsound "player/cloaker/pain";
		hdskin.mug "STF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:PUNG A 0;stop;
	}
}

class HDCulter:HDSkin{
	default{
		hdskin.tauntsound "player/culter/taunt";
		hdskin.xdeathsound "player/culter/terror";
		hdskin.gruntsound "player/culter/pain";
		hdskin.landsound "player/culter/land";
		hdskin.medsound "player/culter/meds";
		hdskin.soundclass "culter";
		deathsound "player/culter/die";
		painsound "player/culter/pain";
		//hdskin.mug "CLT";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDJian:HDCulter
{
	default
	{
		hdskin.tauntsound "player/jian/taunt";
		hdskin.xdeathsound "player/jian/terror";
		hdskin.gruntsound "player/jian/pain";
		hdskin.landsound "player/jian/land";
		hdskin.medsound "player/jian/meds";
		hdskin.soundclass "jian";
		deathsound "player/jian/die";
		painsound "player/jian/pain";
	}
}

class HDFederal:HDCulter
{
	default
	{
		hdskin.tauntsound "player/federal/taunt";
		hdskin.xdeathsound "player/federal/terror";
		hdskin.gruntsound "player/federal/pain";
		hdskin.landsound "player/federal/land";
		hdskin.medsound "player/federal/meds";
		hdskin.soundclass "federal";
		deathsound "player/federal/die";
		painsound "player/federal/pain";
	}
}

class HDBandit:HDCulter
{
	default
	{
		hdskin.tauntsound "player/bandit/taunt";
		hdskin.xdeathsound "player/bandit/terror";
		hdskin.gruntsound "player/bandit/pain";
		hdskin.landsound "player/bandit/land";
		hdskin.medsound "player/bandit/meds";
		hdskin.soundclass "bandit";
		deathsound "player/bandit/die";
		painsound "player/bandit/pain";
	}
}

class HDCultist : HDSkin{
	default{
		hdskin.tauntsound "player/cultist/taunt";
		hdskin.xdeathsound "player/cultist/xdeath";
		hdskin.gruntsound "player/cultist/pain";
		hdskin.landsound "player/cultist/land";
		hdskin.medsound "player/cultist/firepain";
		hdskin.soundclass "cultist";
		deathsound "player/cultist/die";
		painsound "player/cultist/pain";
		xscale 0.5;
		yscale 0.5;
	}
	states{
	spawn:CLTM A 0;stop;
	crouch:CLTC A 0;stop;
	fist:CFRK A 0;stop;
	}
}


class HDKegan:HDSkin{
	default{
		hdskin.tauntsound "player/kegan/taunt";
		hdskin.xdeathsound "player/kegan/xdeath";
		hdskin.gruntsound "player/kegan/grunt";
		hdskin.landsound "player/kegan/land";
		hdskin.medsound "player/kegan/meds";
		hdskin.gibbedsound "player/kegan/gibbed";
		hdskin.stepsound "player/kegan/step";
		hdskin.stepsoundwet "player/kegan/stepwet";
		hdskin.soundclass "kegan";
		deathsound "player/kegan/death";
		painsound "player/kegan/pain";
		hdskin.mug "KGF";
	}
	states{
	spawn:KROG A 0;stop;
	crouch:KRGC A 0;stop;
	fist:KRGS A 0;stop;
	}
}

class HDMorgana:HDSkin{
	default{
		hdskin.tauntsound "player/morgana/taunt";
		hdskin.xdeathsound "player/morgana/xdeath";
		hdskin.gruntsound "player/morgana/grunt";
		hdskin.landsound "player/morgana/land";
		hdskin.medsound "player/morgana/meds";
		hdskin.soundclass "morgana";
		deathsound "player/morgana/death";
		painsound "player/morgana/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

class HDHank:HDSkin{
	default{
		hdskin.tauntsound "player/hank/taunt";
		hdskin.xdeathsound "player/hank/xdeath";
		hdskin.gruntsound "player/hank/grunt";
		hdskin.landsound "player/hank/land";
		hdskin.medsound "player/hank/meds";
		hdskin.soundclass "hank";
		deathsound "player/hank/death";
		painsound "player/hank/pain";
		hdskin.mug "HNK";
		scale 0.5;
	}
	states{
	spawn:HANK A 0;stop;
	crouch:HANK A 0;stop;
	fist:HNKP A 0;stop;
	}
}

class HDMetrocop:HDSkin{
	default{
		hdskin.tauntsound "player/metrocop/taunt";
		hdskin.xdeathsound "player/metrocop/xdeath";
		hdskin.gruntsound "player/metrocop/grunt";
		hdskin.landsound "player/metrocop/land";
		hdskin.medsound "player/metrocop/meds";
		hdskin.soundclass "metrocop";
		deathsound "player/metrocop/death";
		painsound "player/metrocop/pain";
		hdskin.mug "STF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:PUNG A 0;stop;
	}
}


class HDDutch:HDSkin{
	default{
		hdskin.tauntsound "player/dutch/taunt";
		hdskin.xdeathsound "player/dutch/xdeath";
		hdskin.gruntsound "player/dutch/grunt";
		hdskin.landsound "player/dutch/land";
		hdskin.medsound "player/dutch/meds";
		hdskin.soundclass "dutch";
		deathsound "player/dutch/death";
		painsound "player/dutch/pain";
		//hdskin.mug "QGF";
	}
	states{
	spawn:PLAY A 0;stop;
	crouch:PLYC A 0;stop;
	fist:SHTF A 0;stop;
	}
}

//Red Alert 3 Peacekeeper and Tesla Trooper skins by Eric

//shooting the shit
class HDPeacekeeperIdle : HDSkin
{
    default
    {
        hdskin.tauntsound "player/peacekeeper/taunt";
        hdskin.xdeathsound "player/peacekeeper/xdeath";
        hdskin.gruntsound "player/peacekeeper/pain";
        hdskin.medsound "player/peacekeeper/meds";
        hdskin.landsound "player/peacekeeper/land";
        hdskin.soundclass "peacekeeper";
        deathsound "player/peacekeeper/death";
        painsound "player/peacekeeper/pain";
    }
}


//shooting the demons
class HDPeacekeeperAttack : HDSkin
{
    default
    {
        hdskin.tauntsound "player/peacekeeperattack/taunt";
        hdskin.xdeathsound "player/peacekeeperattack/xdeath";
        hdskin.gruntsound "player/peacekeeperattack/pain";
        hdskin.medsound "player/peacekeeperattack/meds";
        hdskin.landsound "player/peacekeeperattack/land";
        hdskin.soundclass "peacekeeperattack";
        deathsound "player/peacekeeperattack/death";
        painsound "player/peacekeeperattack/pain";
    }
}

//holding a position
class HDPeacekeeperDefend : HDSkin
{
    default
    {
        hdskin.tauntsound "player/peacekeeperdefend/taunt";
        hdskin.xdeathsound "player/peacekeeperdefend/xdeath";
        hdskin.gruntsound "player/peacekeeperdefend/pain";
        hdskin.medsound "player/peacekeeperdefend/meds";
        hdskin.landsound "player/peacekeeperdefend/land";
        hdskin.soundclass "peacekeeperdefend";
        deathsound "player/peacekeeperdefend/death";
        painsound "player/peacekeeperdefend/pain";
    }
}

//being shot to shit
class HDPeacekeeperRetreat : HDSkin
{
    default
    {
        hdskin.tauntsound "player/peacekeeperretreat/taunt";
        hdskin.xdeathsound "player/peacekeeperretreat/xdeath";
        hdskin.gruntsound "player/peacekeeperretreat/pain";
        hdskin.medsound "player/peacekeeperretreat/meds";
        hdskin.landsound "player/peacekeeperretreat/land";
        hdskin.soundclass "peacekeeperretreat";
        deathsound "player/peacekeeperretreat/death";
        painsound "player/peacekeeperretreat/pain";
    }
}

class HDTeslaTrooperIdle : HDSkin
{
    default
    {
        hdskin.tauntsound "player/teslatrooper/taunt";
        hdskin.xdeathsound "player/teslatrooper/xdeath";
        hdskin.gruntsound "player/teslatrooper/pain";
        hdskin.medsound "player/teslatrooper/meds";
        hdskin.landsound "player/teslatrooper/land";
        hdskin.soundclass "teslatrooper";
        deathsound "player/teslatrooper/death";
        painsound "player/teslatrooper/pain";
    }
}

class HDTeslaTrooperAttack : HDSkin
{
    default
    {
        hdskin.tauntsound "player/teslatrooperattack/taunt";
        hdskin.xdeathsound "player/teslatrooperattack/xdeath";
        hdskin.gruntsound "player/teslatrooperattack/pain";
        hdskin.medsound "player/teslatrooperattack/meds";
        hdskin.landsound "player/teslatrooperattack/land";
        hdskin.soundclass "teslatrooperattack";
        deathsound "player/teslatrooperattack/death";
        painsound "player/teslatrooperattack/pain";
    }
}

class HDTeslaTrooperDefend : HDSkin
{
    default
    {
        hdskin.tauntsound "player/teslatrooperdefend/taunt";
        hdskin.xdeathsound "player/teslatrooperdefend/xdeath";
        hdskin.gruntsound "player/teslatrooperdefend/pain";
        hdskin.medsound "player/teslatrooperdefend/meds";
        hdskin.landsound "player/teslatrooperdefend/land";
        hdskin.soundclass "teslatrooperdefend";
        deathsound "player/teslatrooperdefend/death";
        painsound "player/teslatrooperdefend/pain";
    }
}

class HDTeslaTrooperRetreat : HDSkin
{
    default
    {
        hdskin.tauntsound "player/teslatrooperretreat/taunt";
        hdskin.xdeathsound "player/teslatrooperretreat/xdeath";
        hdskin.gruntsound "player/teslatrooperretreat/pain";
        hdskin.medsound "player/teslatrooperretreat/meds";
        hdskin.landsound "player/teslatrooperretreat/land";
        hdskin.soundclass "teslatrooperretreat";
        deathsound "player/teslatrooperretreat/death";
        painsound "player/teslatrooperretreat/pain";
    }
}

class HDPeppino:HDSkin{
	default{
		hdskin.tauntsound "player/peppino/taunt";
		hdskin.xdeathsound "player/peppino/xdeath";
		hdskin.gibbedsound  "player/peppino/gibbed";
		hdskin.gruntsound "player/peppino/grunt";
		hdskin.landsound "player/peppino/land";
		hdskin.medsound "player/peppino/meds";
		hdskin.soundclass "peppino";
		hdskin.stepsound "player/peppino/step";
		hdskin.stepsoundwet "player/peppino/step";
		deathsound "player/peppino/death";
		painsound "player/peppino/pain";
		hdskin.mug "PPN";
		hdskin.minvpitch -1;
		hdskin.maxvpitch -1;
	}
	states{
	spawn:PEPP A 0;stop;
	crouch:PEPP A 0;stop;
	fist:00PP A 0;stop;
	}
}

class HDPeppinoSpecificTaunt:HDPeppino{
	default{
		hdskin.tauntsound "player/peppinospecifictaunt/taunt";
	}
}

//only use this if you're willing to set up a series of very specific aliases and binds
class HDPeppinoEvenMoreSpecificTaunt:HDPeppino{
	default{
		hdskin.tauntsound "player/peppinospecifictaunt/taunt";
	}	
	states{
	spawn:PEPT A 0;stop;
	crouch:PEPT A 0;stop;
	fist:00PP A 0;stop;
	}
}

class HDLydia : HDSkin{
	default{
		hdskin.tauntsound "player/lydia/taunt";
		hdskin.xdeathsound "player/hdgal/xdeath";
		hdskin.gruntsound "player/lydia/grunt";
		hdskin.landsound "player/lydia/land";
		hdskin.medsound "player/lydia/meds";
		hdskin.soundclass "lydia";
		hdskin.mug "LYF";
		deathsound "player/lydia/die";
		painsound "player/lydia/pain";
	}
	states{
	spawn:LYDI A 0;stop;
	crouch:LYDC A 0;stop;
	fist:LYDP A 0;stop;
	}
}
